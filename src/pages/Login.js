import React, { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import { FaUserAlt, FaLock, FaFacebook, FaGoogle, FaLinkedinIn, FaTwitter } from 'react-icons/fa';
import UserContext from '../UserContext';
 
 
const Login = (props) => {
 
//    const history = useNavigate();
   const { user, setUser } = useContext(UserContext);
 
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");
   const [willRedirect, setWillRedirect] = useState(false);
 
   const authenticate = (e) => {
 
       e.preventDefault();
 
       fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({
               email: email,
               password: password
           })
       })
       .then(res => res.json())
       .then(data => {
 
           if (typeof data.access !== 'undefined') {
               localStorage.setItem('token', data.access);
               retrieveUserDetails(data.access);
           } else {
               alert("Authentication failed. Please check your login details and try again.");
           }
 
       })
   }
  
   const retrieveUserDetails = (token) => {
 
       fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
           headers: { Authorization: `Bearer ${ token }`}
       })
       .then(res => res.json())
       .then(data => {
 
           setUser({ id: data._id, isAdmin: data.isAdmin });
 
           if (data.isAdmin === true) {
               setWillRedirect(true);
           } else {
            //    if (props.location.state.from === 'cart') {
                //    history.goBack();
            //    } else {
                   setWillRedirect(true);
            //    }
           }
       })
   }
 
   return(
       willRedirect === true ?
           user.isAdmin === true ?
               <Navigate to='/products'/>
           :
               <Navigate to='/'/>
       :
           <Row className="justify-content-center">
               <Col xs md="6" className='logBody'>
                   <Card>
                       <Form className='logForm' onSubmit={e => authenticate(e)}>
                       <h2 className="text-center my-4 logTitle">Log In</h2>
                           <Card.Body>
                               <Form.Group controlId="userEmail" className='formGroup'>
                                   <Form.Label><FaUserAlt /></Form.Label>
                                   <Form.Control
                                       type="email"
                                       placeholder="Enter your email"
                                       value={email}
                                       onChange={e => setEmail(e.target.value)}
                                       required
                                   />
                               </Form.Group>
 
                               <Form.Group controlId="password" className='formGroup'>
                                   <Form.Label><FaLock /></Form.Label>
                                   <Form.Control
                                       type="password"
                                       placeholder="Enter your password"
                                       value={password}
                                       onChange={e => setPassword(e.target.value)}
                                       required
                                   />
                               </Form.Group>
 
                           </Card.Body>
                           <Card.Footer>
                               <Button variant="primary" type="submit" className='btnSubmit'>
                                   Submit
                               </Button>
                               <p className='sOption'>or sign in with social platform</p>
                               <hr className='horizontalRule'></hr>
                               <div>
                                   <FaFacebook className='sIcons' />
                                   <FaGoogle className='sIcons' />
                                   <FaTwitter className='sIcons' />
                                   <FaLinkedinIn className='sIcons' />

                               </div>
                           </Card.Footer>
                           <p className="text-center mt-3">
                       Don't have an account yet? <Link to="/register">Click here</Link> to register.
                   </p>
                       </Form>
                   </Card>
               </Col>             
           </Row>
   );
  
}
export default Login;