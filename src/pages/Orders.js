import React, { useState, useEffect } from 'react';
import { Container, Card, Accordion } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default function Orders(){

	const [ordersList, setOrdersList] = useState([]);

	useEffect(()=> {

		fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if (data.length > 0) {

				let orders = data.map((item, index)=> {

					return(
						<Card key={item._id}>
							<Accordion 
								as={Card.Header}
								eventkey={index + 1}
								className="bg-secondary text-white"
							>
								Order #{index + 1} - Purchased on: {moment(item.purchasedOn).format("MM-DD-YYYY")} (Click for Details)
							</Accordion>
							<Accordion eventkey={index + 1}>
								<Card.Body>
									<h6>Items:</h6>
									<ul>
									{
										item.products.map((subitem) => {

											fetch(`${process.env.REACT_APP_API_URL}/products/${subitem.productId}`,{
												headers:{
													'Content-Type': 'application/json',
													'Accept': 'application/json'
												}
											})
											.then(res => res.json())
											.then(data => {
											});

											return (
												<li key={subitem._id}>
													{subitem.productName} - Quantity: {subitem.quantity}
												</li>
											);

										})

									}
									</ul>
									<h6>
										Total: <span className="text-warning">₱{item.totalAmount}</span>
									</h6>
								</Card.Body>
							</Accordion>
						</Card>
					)
				})
	
				setOrdersList(orders);				
			}
		})

	}, []);

	return(
		ordersList.length === 0 ?
			<div>
					<h3 className="text-center">
						No orders placed yet! <Link to="/products">Start shopping.</Link>
					</h3>
			</div>
		:
		<Container className='orderHistory'>
			<h2 className="text-center my-4">Order History</h2>
			<Accordion>
				{ordersList}
			</Accordion>
		</Container>
	)
}