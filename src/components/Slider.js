import React, { useEffect, useState } from "react";
import SliderContent from "./SliderContent";
import Arrows from "./Arrows";
import Dots from "./Dots";
import "./slider.css";
import imageSlider from "./imageSlider";

const len = imageSlider.length -1;
const Slider = (props) =>{

    const [activeIndex, setActiveIndex] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setActiveIndex(activeIndex === len ? 0 : activeIndex + 1);
        }, 3000);
        return () => clearInterval(interval);
    }, [activeIndex]);
    return(
        <div className="slider-container">
            <SliderContent activeIndex={activeIndex} imageSlider={imageSlider}/>
            <Arrows
             prevSlide={() => 
                setActiveIndex(activeIndex < 1 ? len : activeIndex - 1)
                }
            nextSlide={() => 
                setActiveIndex(activeIndex === 0 ? len : activeIndex + 1)
                } />
            <Dots activeIndex={activeIndex} imageSlider={imageSlider} onClick={(activeIndex) => setActiveIndex(activeIndex)} />
        </div>
    );
};

export default Slider;

