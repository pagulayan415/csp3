import First from './../assets/cs4.jpg';
import Second from './../assets/cs2.jpg';
import Third from './../assets/cs3.jpg';
import Fourth from './../assets/cs5.png';
import Fifth from './../assets/cs6.jpg';
import Sixth from './../assets/cs7.jpg';
import Seventh from './../assets/cs8.jpg';
import Eight from './../assets/cs9.jpg';

export default [{
    urls: First,    
},
{
    urls: Second,
},
{
    urls: Third,
},
{
    urls: Fourth,
},
{
    urls: Fifth,
},
{
    urls: Sixth,
},
{
    urls: Seventh,
},
{
    urls: Eight,
}];